# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the UserListView
    url(
        r'^$',
        view=views.StoreListView.as_view(),
        name='list'
    ),
    url(
        r'^add/$',
        view=views.StoreCreateView.as_view(),
        name='create'
    ),
    url(
        r'^(?P<pk>\d+)/$',
        view=views.StoreDetailView.as_view(),
        name='detail'
    ),

    url(
        r'^(?P<pk>\d+)/update/$',
        view=views.StoreUpdateView.as_view(),
        name='update'
    )
]
