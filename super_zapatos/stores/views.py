# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic import DetailView, ListView, CreateView, UpdateView


from .models import Store, Article
from .mixins import StoreArticleListMixin


class StoreListView(ListView):
    """
    This is the Store list view
    """
    model = Store
    template_name = 'stores/store_list.html'


class StoreCreateView(CreateView):
    """
    This is the Store create view
    """
    model = Store
    fields = ['name', 'address']
    template_name = 'stores/store_form.html'
    success_url = reverse_lazy('stores:list')


class StoreDetailView(DetailView):
    """
    This is the Store detail view
    """
    model = Store
    template_name = 'stores/store_detail.html'


class StoreUpdateView(UpdateView):
    """
    This is the Store update view
    """
    model = Store
    fields = ['name', 'address']
    template_name = 'stores/store_form.html'


class ArticleListView(ListView):
    """
    This is the Article list view
    """
    model = Article
    template_name = 'stores/article_list.html'


class ArticleCreateView(CreateView):
    """
    This is the Article create view
    """
    model = Article
    template_name = 'stores/article_form.html'
    fields = ['name', 'description', 'price', 'total_in_shelf', 'total_in_vault', 'store']


class ArticleDetailView(DetailView):
    """
    This is the Article detail view
    """
    model = Article
    template_name = 'stores/article_detail.html'


class ArticleUpdateView(UpdateView):
    """
    This is the Article update view
    """
    model = Article
    template_name = 'stores/article_form.html'
    fields = ['name', 'description', 'price', 'total_in_shelf', 'total_in_vault', 'store']


class StoreArticlesListView(StoreArticleListMixin, ListView):
    """
    This is the Article per Store list view
    """
    model = Article
    template_name = 'stores/store_article_list.html'
