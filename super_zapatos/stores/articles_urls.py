# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^$',
        view=views.ArticleListView.as_view(),
        name='list'
    ),
    url(
        r'^add/$',
        view=views.ArticleCreateView.as_view(),
        name='create'
    ),

    url(
        r'^(?P<pk>\d+)/$',
        view=views.ArticleDetailView.as_view(),
        name='detail'
    ),
    url(
        r'^(?P<pk>\d+)/update/$',
        view=views.ArticleUpdateView.as_view(),
        name='update'
    ),
    url(
        r'^stores/(?P<pk>\d+)/$',
        view=views.StoreArticlesListView.as_view(),
        name='stores'
    )
]

