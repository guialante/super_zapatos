# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from .models import Store


class StoreArticleListMixin(object):
    """
    This mixin allow to load the store object in the context and filter the articles by store
    """

    def get_context_data(self, **kwargs):
        context = super(StoreArticleListMixin, self).get_context_data(**kwargs)
        store = Store.objects.get(pk=self.kwargs.get('pk'))
        context['store'] = store
        return context

    def get_queryset(self):
        queryset = super(StoreArticleListMixin, self).get_queryset()

        queryset = queryset.select_related('store').filter(pk=self.kwargs.get('pk'))
        return queryset
