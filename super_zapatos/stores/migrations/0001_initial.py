# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=255, verbose_name='Name of Article')),
                ('description', models.TextField(default='', verbose_name='Description')),
                ('price', models.PositiveIntegerField(verbose_name='Price')),
                ('total_in_shelf', models.PositiveIntegerField(verbose_name='Total in Shelf')),
                ('total_in_vault', models.PositiveIntegerField(verbose_name='total in Vault')),
            ],
            options={
                'db_table': 'articles',
            },
        ),
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=255, verbose_name='Name of Store')),
                ('address', models.CharField(default='', max_length=255, verbose_name='Address of Store')),
            ],
            options={
                'db_table': 'stores',
            },
        ),
        migrations.AddField(
            model_name='article',
            name='store',
            field=models.ForeignKey(related_name='articles', to='stores.Store'),
        ),
    ]
