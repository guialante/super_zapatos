# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class Store(models.Model):
    """
    This model represents the store table and objects
    """
    name = models.CharField(_("Name of Store"), max_length=255, default='')
    address = models.CharField(_("Address of Store"), max_length=255, default='')

    class Meta:
        db_table = 'stores'
        verbose_name = 'store'
        verbose_name_plural = 'stores'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('stores:detail', kwargs={'pk': self.pk})


@python_2_unicode_compatible
class Article(models.Model):
    """
    This model represents the article table and objects
    """
    name = models.CharField(_("Name of Article"), max_length=255, default='')
    description = models.TextField(_("Description"), default='')
    price = models.PositiveIntegerField(_("Price"))
    total_in_shelf = models.PositiveIntegerField(_("Total in Shelf"))
    total_in_vault = models.PositiveIntegerField(_("total in Vault"))
    store = models.ForeignKey(Store, related_name='articles')

    class Meta:
        db_table = 'articles'
        verbose_name = 'article'
        verbose_name_plural = 'articles'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('articles:detail', kwargs={'pk': self.pk})
