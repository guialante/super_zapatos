# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework import serializers

from .models import Store, Article


class StoreSerializer(serializers.ModelSerializer):
    """
    This is the serializer for the store services API
    """
    class Meta:
        model = Store
        fields = ['id', 'address', 'name']


class ArticleSerializer(serializers.ModelSerializer):
    """
    This is the serializer for the article services API
    """
    store_name = serializers.ReadOnlyField(source='store.name')

    class Meta:
        model = Article
        fields = ['id', 'name', 'description', 'price', 'total_in_shelf', 'total_in_vault', 'store_name']



