# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import logging

from django.http import Http404

from rest_framework import generics
from rest_framework.exceptions import ParseError

from .mixins import BasicAuthenticationMixin
from super_zapatos.stores import serializers
from super_zapatos.stores import models

logger = logging.getLogger(__name__)


class StoreListApiView(BasicAuthenticationMixin, generics.ListAPIView):
    """
    This is the store list for the API services
    """
    queryset = models.Store.objects.all()
    serializer_class = serializers.StoreSerializer


class ArticleListApiView(BasicAuthenticationMixin, generics.ListAPIView):
    """
    This is the article list for the API services
    """
    queryset = models.Article.objects.all()
    serializer_class = serializers.ArticleSerializer


class ArticleStoreListApiView(BasicAuthenticationMixin, generics.ListAPIView):
    """
    This is the article per store list for the API services
    """
    queryset = models.Article.objects.all()
    serializer_class = serializers.ArticleSerializer

    def get_queryset(self):
        queryset = super(ArticleStoreListApiView, self).get_queryset()
        pk = self.kwargs.get(self.lookup_field)
        try:
            pk = int(pk)
            queryset = queryset.filter(store__pk=pk)
            if queryset:
                return queryset
            else:
                raise Http404
        except ValueError:
            raise ParseError
