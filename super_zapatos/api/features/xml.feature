Feature: Testing XML web services on '/articles/stores/id' endpoint

  Scenario: call web service with valid store ID record
      Given call with valid url
      Then XML service should status_code 200

#  Scenario: call web service with not valid store ID record
#      when I call with not valid record ID
#      then XML service should return 404 status code
#
#  Scenario: call web service with not number store ID record
#      when I call with not number record ID
#      then XML service should return 400 status code
