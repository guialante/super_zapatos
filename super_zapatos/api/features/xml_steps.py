# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import requests
from nose.tools import eq_
from lettuce import *


@step('call with valid url')
def call_with_valid_url(step):
    request = requests.get('http://localhost:8000/services/articles/stores/1')
    world.status_code = request.status_code


@step(' XML service should status_code 200')
def service_return(step):
    eq_(world.status_code, 200)
