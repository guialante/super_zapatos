# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [

    url(
        r'^stores/?$',
        view=views.StoreListApiView.as_view(),
        name='list'
    ),
    url(
        r'^articles/?$',
        view=views.ArticleListApiView.as_view(),
        name='articles_list'
    ),
    url(
        r'^articles/stores/(?P<pk>\w+)/?$',
        view=views.ArticleStoreListApiView.as_view(),
        name='articles_store_list'
    ),


]
