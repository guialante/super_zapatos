# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import logging
import base64

from rest_framework import HTTP_HEADER_ENCODING
from rest_framework.exceptions import AuthenticationFailed

logger = logging.getLogger(__name__)


class BasicAuthenticationMixin(object):
    """
    This mixin allow to implement the Basic authentication requirement and is used by all API views
    """
    def get(self, request, *args, **kwargs):
        frmt = request.query_params.get('format')

        if frmt is not None and frmt == 'json':
            auth = request.META.get('HTTP_AUTHORIZATION', b'')
            auth = auth.encode(HTTP_HEADER_ENCODING)
            auth = auth.split()

            if not auth or auth[0].lower() != b'basic':
                raise AuthenticationFailed()

            if len(auth) == 1:
                raise AuthenticationFailed()
            elif len(auth) > 2:
                raise AuthenticationFailed()
            try:
                auth_parts = base64.b64decode(auth[1]).decode(HTTP_HEADER_ENCODING).partition(':')
            except (TypeError, UnicodeDecodeError):
                raise AuthenticationFailed()

            userid, password = auth_parts[0], auth_parts[2]
            logger.debug(password)
            if userid != 'my_user' or password != 'my_password':
                raise AuthenticationFailed()

        return super(BasicAuthenticationMixin, self).get(request, *args, **kwargs)
