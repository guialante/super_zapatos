# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import logging

from rest_framework import status
from rest_framework_xml.renderers import XMLRenderer
from rest_framework.renderers import JSONRenderer
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


class CustomXMLRenderer(XMLRenderer):
    """
    This render allow to show the XML data according the requirements
    """

    def render(self, data, accepted_media_type=None, renderer_context=None):

        view = renderer_context.get('view')
        total_elements = len(data)
        status_code = view.response.status_code

        if total_elements > 1:
            self.item_tag_name = view.queryset.model._meta.verbose_name_plural
        else:
            self.item_tag_name = view.queryset.model._meta.verbose_name
        ret = super(CustomXMLRenderer, self).render(data, accepted_media_type=None, renderer_context=None)

        soup = BeautifulSoup(ret, "html.parser")
        success_tag = soup.new_tag("success")

        if status_code == status.HTTP_200_OK:
            success_tag.string = "true"
            soup.root.append(success_tag)
            if total_elements > 1:
                total_elements_tag = soup.new_tag("total_elements")
                total_elements_tag.string = str(total_elements)
                soup.root.append(total_elements_tag)
        else:
            success_tag.string = "false"
            error_code_tag = soup.new_tag("error_code")
            error_message_tag = soup.new_tag("error_msg")
            if status_code == status.HTTP_400_BAD_REQUEST:
                error_code_tag.string = "400"
                error_message_tag.string = "Bad Request"

            elif status_code == status.HTTP_404_NOT_FOUND:
                error_code_tag.string = "404"
                error_message_tag.string = "Record Not Found"

            elif status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                error_code_tag.string = "500"
                error_message_tag.string = "Server Error"

            for tag in soup.find_all('stores'):
                tag.decompose()

            if soup.find('detail') is not None:
                soup.detail.decompose()

            soup.root.append(success_tag)
            soup.root.append(error_code_tag)
            soup.root.append(error_message_tag)
        ret = str(soup)
        return ret


class CustomJSONRenderer(JSONRenderer):
    """
    This render allow to show the JSON data according the requirements
    """

    def render(self, data, accepted_media_type=None, renderer_context=None):
        view = renderer_context.get('view')
        total_elements = len(data)
        status_code = view.response.status_code
        if total_elements > 1:
            model_name = view.queryset.model._meta.verbose_name_plural
        else:
            model_name = view.queryset.model._meta.verbose_name
        ret = super(CustomJSONRenderer, self).render(data, accepted_media_type=None, renderer_context=None)

        if status_code == status.HTTP_200_OK:
            if total_elements > 1:
                ret = '{"%s":%s, "success":true, "total_elements":%s}' % (model_name, ret, total_elements)
            else:
                ret = '{"%s":%s, "success":true}' % (
                    model_name, ret.replace('[', '').replace(']', '')
                )
        else:
            if status_code == status.HTTP_400_BAD_REQUEST:
                error_code = "400"
                error_message = "Bad Request"
            if status_code == status.HTTP_401_UNAUTHORIZED or status_code == status.HTTP_403_FORBIDDEN:
                error_code = "401"
                error_message = "Not Authorized"
            elif status_code == status.HTTP_404_NOT_FOUND:
                error_code = "404"
                error_message = "Record Not Found"
            elif status_code == status.HTTP_500_INTERNAL_SERVER_ERROR:
                error_code = "500"
                error_message = "Server Error"
            ret = '{"success": false, "error_code":%s, "error_msg":"%s"}' % (error_code, error_message)

        return bytes(ret.encode('utf-8'))
