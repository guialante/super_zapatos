Feature: Testing JSON web services on '/articles/stores/id' endpoint

   Scenario: call web service with valid store ID record with valid auth credentials
      when I call with valid record ID with valid auth credentials
      then JSON service should return 200 status code

   Scenario: call web service with not valid store ID record with valid auth credentials
      when I call with not valid record ID with auth credentials
      then JSON service should return 404 status code

  Scenario: call web service with valid store ID record without auth credentials
      when I call with valid record ID without auth credentials
      then JSON service should return 401 status code

  Scenario: call web service with not valid store ID record without auth credentials
      when I call with not valid record ID without auth credentials
      then JSON service should return 401 status code instead 404

  Scenario: call web service with not number store ID record with valid auth credentials
      when I call with not number record ID with valid auth credentials
      then JSON service should return 400 status code

  Scenario: call web service with valid store ID record with not valid auth credentials
      when I call with valid record ID with not valid auth credentials
      then JSON service should return 401 status code instead 200
