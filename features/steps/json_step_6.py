# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import requests
from nose.tools import eq_
from behave import then, when

auth = ('my_user', '123456')


@when('I call with valid record ID with not valid auth credentials')
def step_impl(context):
    request = requests.get('http://localhost:8000/services/articles/stores/1', params={'format': 'json'}, auth=auth)
    context.status_code = request.status_code


@then('JSON service should return 401 status code instead 200')
def step_impl(context):
    eq_(context.status_code, 403)
