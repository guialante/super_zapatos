# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import requests
from nose.tools import eq_
from behave import then, when

auth = ('my_user', 'my_password')


@when('I call with not valid record ID with auth credentials')
def step_impl(context):
    request = requests.get('http://localhost:8000/services/articles/stores/2', params={'format': 'json'}, auth=auth)
    context.status_code = request.status_code


@then('JSON service should return 404 status code')
def step_impl(context):
    eq_(context.status_code, 404)
