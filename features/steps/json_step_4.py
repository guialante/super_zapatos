# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import requests
from nose.tools import eq_
from behave import then, when


@when('I call with not valid record ID without auth credentials')
def step_impl(context):
    request = requests.get('http://localhost:8000/services/articles/stores/2', params={'format': 'json'})
    context.status_code = request.status_code


@then('JSON service should return 401 status code instead 404')
def step_impl(context):
    eq_(context.status_code, 403)
